import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/Welcome'
import About from '@/components/About'
import Preferences from '@/components/Preferences'
import Resources from '@/components/Resources'
import Tools from '@/components/Tools'
import Help from '@/components/Help'
import Versions from '@/components/Versions'
import Open from '@/components/Open'
import OpenURL from '@/components/OpenURL'
import SaveAs from '@/components/SaveAs'
import Cloud from '@/components/Cloud'
import Highlighters from '@/components/Highlighters'
import WordNGrams from '@/components/WordNGrams'
import CharNGrams from '@/components/CharNGrams'
import Dictionaries from '@/components/Dictionaries'
import Sorting from '@/components/Sorting'
import Combine from '@/components/Combine'
import Managing from '@/components/Managing'
import AboutText from '@/components/AboutText'
import ShowStatistics from '@/components/ShowStatistics'
import WordRelations from '@/components/WordRelations'
import InlineFrame from '@/components/InlineFrame'
import Nominatim from '@/components/Nominatim'
import Calendar from '@/components/Calendar'
import GUI from '@/components/GUI'
import Documents from '@/components/Documents'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/welcome',
      name: 'Welcome',
      component: Welcome
    },
    {
      path: '/about',
      name: 'About',
      component: About
    },
    {
      path: '/preferences',
      name: 'Preferences',
      component: Preferences
    },
    {
      path: '/resources',
      name: 'Resources',
      component: Resources
    },
    {
      path: '/tools',
      name: 'Tools',
      component: Tools
    },
    {
      path: '/help',
      name: 'Help',
      component: Help
    },
    {
      path: '/versions',
      name: 'Versions',
      component: Versions
    },
    {
      path: '/open',
      name: 'Open',
      component: Open
    },
    {
      path: '/openURL',
      name: 'OpenURL',
      component: OpenURL
    },
    {
      path: '/saveas',
      name: 'SaveAs',
      component: SaveAs
    },
    {
      path: '/cloud',
      name: 'Cloud',
      component: Cloud
    },
    {
      path: '/highlighters',
      name: 'Highlighters',
      component: Highlighters
    },
    {
      path: '/wordngrams',
      name: 'WordNGrams',
      component: WordNGrams
    },
    {
      path: '/charngrams',
      name: 'CharNGrams',
      component: CharNGrams
    },
    {
      path: '/dictionaries',
      name: 'Dictionaries',
      component: Dictionaries
    },
    {
      path: '/sorting',
      name: 'Sorting',
      component: Sorting
    },
    {
      path: '/combine',
      name: 'Combine',
      component: Combine
    },
    {
      path: '/managing',
      name: 'Managing',
      component: Managing
    },
    {
      path: '/about-text',
      name: 'AboutText',
      component: AboutText
    },
    {
      path: '/show-statistics',
      name: 'ShowStatistics',
      component: ShowStatistics
    },
    {
      path: '/word-relations',
      name: 'WordRelations',
      component: WordRelations
    },
    {
      path: '/inline-frame',
      name: 'InlineFrame',
      component: InlineFrame
    },
    {
      path: '/nominatim',
      name: 'Nominatim',
      component: Nominatim
    },
    {
      path: '/calendar',
      name: 'Calendar',
      component: Calendar
    },
    {
      path: '/documents',
      name: 'Documents',
      component: Documents
    }
  ]
})
