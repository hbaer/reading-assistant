/*
 * Table Display.
 */

module.exports = function(scrollPanel, contentPanel) {

  scrollPanel = resolveElement(scrollPanel);
  contentPanel = resolveElement(contentPanel);

  var lineHeight = 20,
      elements = [],
      grid,
      lowerBound = 0,
      upperBound = 0,
      gridHeight = 0,
      panelHeight = 0,
      numLines = 0,
      currentView;

  function createCurrentView() {
    currentView = document.createElement('div');
    currentView.style.position = 'absolute';
    currentView.style.width = '100%';
    contentPanel.appendChild(currentView);
  }

  function update() {
    var scrollPanelStyle = window.getComputedStyle(scrollPanel);
    lineHeight = parseFloat(scrollPanelStyle.getPropertyValue('line-height'));
    panelHeight = parseFloat(scrollPanelStyle.getPropertyValue('height'));
    if (grid) {
      gridHeight = grid.length * lineHeight;
      contentPanel.style.height = gridHeight + 'px';
      numLines = Math.ceil(panelHeight / lineHeight);
      draw();
    }
  };

  createCurrentView();
  update();

  // Resize
  window.addEventListener('resize', update);

  function setScrollTop(line) {
    scrollPanel.scrollTop = lineHeight * line;
    draw();
  };

  function getScrollTop() {
    return Math.min(scrollPanel.scrollTop, gridHeight - panelHeight - 1);
  };

  function getScrollLine() {
    return Math.max((getScrollTop() / lineHeight) | 0, 0);
  }

  function prependElements(from, to) {
    var ref = currentView.firstChild;
    for (var i = from; i < to; ++i) {
      var element = grid[i];
      currentView.insertBefore(element, ref);
    }
  };

  function appendElements(from, to) {
    for (var i = from; i < to; ++i) {
      var element = grid[i];
      currentView.appendChild(element);
    }
  };

  function removeElements(from, to) {
    for (var i = from; i < to; ++i) {
      var element = grid[i];
      element.removeAttribute('title');
      if (currentView.hasChildNodes()) {
        currentView.removeChild(element);
      }
    }
  };

  function draw() {
    var element;
    var viewTop = Math.max(getScrollTop(), 0);
    var lb = getScrollLine();
    var ub = Math.min(lb + numLines, grid.length);

    currentView.style.top = viewTop + 'px';

    if (lb < lowerBound) {
      prependElements(lb, Math.min(lowerBound, ub));
    }
    else if (lb > lowerBound) {
      removeElements(lowerBound, Math.min(lb, lowerBound + numLines));
    }

    if (ub > upperBound) {
      appendElements(Math.max(upperBound, lb), ub)
    }
    else if (ub < upperBound) {
      removeElements(Math.max(ub, upperBound - numLines), upperBound);
    }

    lowerBound = lb;
    upperBound = ub;
  };

  function reset() {
    while (currentView.firstChild) {
      currentView.removeChild(currentView.firstChild);
    }
  }

  function removeAllRows() {
    reset();
    lowerBound = upperBound = 0;
    grid = null;
    gridHeight = 0;
    scrollPanel.scrollTop = 0;
  }

  // Scroll
  scrollPanel.addEventListener('scroll', draw);

  function resolveElement(sel) {
    return typeof(sel) == 'string' ? document.querySelector(sel) : sel;
  };

  return {
    setData: function(_) { removeAllRows(); grid = _; update(); },
    getData: function() { return grid },
    update: function() { update(); },
    setLineHeight: function(_) { lineHeight = _ },
    removeAllRows: function() { removeAllRows() },
    reset: function() { reset(); upperBound = lowerBound; update() },
    scrollToIndex: function(index) { setScrollTop(index) }
  };

}
