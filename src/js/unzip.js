/*
 * Get content from zip file
 */

module.exports = function (data, callback) {

  const UNZIP = require('isomorphic-unzip');
  const MIME = require('mime-types');
  const PATH = require('path-browserify');

  function getZIP(data, callback) {
    var zip = [];
    var unzip = new UNZIP(data);
    var promises = [];
    unzip.getEntries(function(err, entries) {
      entries.forEach(entry => {
        promises.push(new Promise((resolve, reject) => {
          UNZIP.getEntryData(entry, function(err, blob) {
            var type = MIME.lookup(entry.filename);
            var modBlob = new Blob([blob.slice()], { type: type });
            modBlob.name = entry.filename;
            resolve(modBlob);
          });
        }));
      });
      Promise.all(promises).then(zip => {
        callback(zip);
      });
    });
  }

  var blob = new Blob([data]);

  getZIP(blob, zip => {

    var find = function(zip) {
      return function(compare) {
        return zip.find(compare);
      }
    }(zip);

    var filename = function(path) {
      return path.substr(path.lastIndexOf('/') + 1);
    };

    var subPath = function(path) {
      return path.replace(/^.*images\//, '');
    }

    var common = function(p1, p2, f) {
      return f(p1) === f(p2);
    }

    function load(blob, callback) {
      var reader = new FileReader();

      reader.onload = function(e) {
        callback(e.target.result);
      }

      if (blob.type.match(/^image\/.*/)) {
        reader.readAsBinary(blob);
      }
      else {
        reader.readAsText(blob);
      }

    }

    var text = find(e => { return filename(e.name) == 'text.html'});

    if (text) {
      load(text, data => {

        var parser = new DOMParser();
        var text = parser.parseFromString(data, "text/html");

        // Resolve included images
        var imgs = text.querySelectorAll('img');
        Array.from(imgs).forEach((img, i) => {
          var blob = find(e => { return common(img.src, e.name, subPath) });
          if (blob) {
            img.setAttribute('data-filename', PATH.basename(img.src));
            img.setAttribute('src', window.URL.createObjectURL(blob));
          }
        });

        // Resolve SVG images
        var images = text.querySelectorAll('image');
        Array.from(images).forEach(image => {
          var href = image.getAttribute('xlink:href');
          var blob = find(e => {
            return common(e.name, href, subPath)
          });
          if (blob) {
            image.setAttribute('data-filename', PATH.basename(href));
            image.setAttribute('xlink:href', window.URL.createObjectURL(blob));
          }
        });

        var dict = find(e => { return filename(e.name) == 'dict.html' });
        if (dict) {
          load(dict, data => {
            var parser = new DOMParser();
            var dict = parser.parseFromString(data, 'text/html');
            var json = find(e => { return filename(e.name) == 'data.json'});
            if (json) {
              load(json, data => {
                callback(text, dict, data);
              });
            }
            else {
              callback(text, dict);
            }
          });
        }
        else {
          callback(text);
        }
      })
    }
    else {
      zip.forEach((blob, i) => {
        if (blob.type !== 'false' && blob.name.indexOf('__MACOSX') != 0) {
          blob.name = filename(blob.name);
          callback(null, null, blob);
        }
      })
    }

  });

}
