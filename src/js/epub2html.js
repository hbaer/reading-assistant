/*
 * Get HTML and images from a EPUB file.
 */

module.exports = function (data, callback) {

  const UNZIP = require('isomorphic-unzip');
  const PATH = require('path-browserify');

  function getEPUB(data, callback) {
    var epub = [];
    var unzip = new UNZIP(data);
    var promises = [];
    unzip.getEntries(function(err, entries) {
      entries.forEach(entry => {
        promises.push(new Promise((resolve, reject) => {
          UNZIP.getEntryData(entry, function(err, blob) {
            resolve({ filename: entry.filename, blob: blob});
          });
        }));
      });
      Promise.all(promises).then(epub => {
        callback(epub);
      });
    });
  }


  var blob = new Blob([data]);
  getEPUB(blob, function(epub) {


    // Find ZIP entry
    var find = function(epub) {
      return function (name) {
        var unescapedName = window.unescape(name);
        return epub.find(e => { return e.filename == unescapedName });
      }
    }(epub);

    // Get blob from ZIP entry
    function findBlob(name) {
      var entry = find(name);
      return entry ? entry.blob : null;
    }


    // Create a promise for accessing the text of a blob
    // including an additional parameter.
    function getTextPromise(blob, param) {
      return new Promise((resolve, reject) => {
        var reader = new FileReader();
        reader.onloadend = function(e) {
          resolve({ text: this.result, param: param });
        }
        reader.readAsText(blob);
      });
    }


    // Process css styles
    function processStyles(styles) {
      var styles = styles.replace(/\/\*(.|\n)*?\*\//g, '').trim();
      var selectors = {};
      var p0 = 0, p1 = styles.indexOf('{'), p2;
      while (p1 != -1) {
        p2 = styles.indexOf('}', p1);
        var sel = styles.substring(p0, p1).trim();
        var val = styles.substring(p1, ++p2).trim();
        p1 = styles.indexOf('{', p0 = p2);
        if (!selectors[sel]) {
          selectors[sel] = val;
        };
      };
      return selectors;
    };


    // Transform selector epub namespace
    function transformEpubNamespaceSelectors(selector) {
      return selector.replace(/epub\|/g, 'data-epub_');
    }


    // Transform attribute epub namespace
    function transformEpubNamespaceAttributes(doc) {
      var elements = doc.querySelectorAll('body *');
      Array.from(elements).forEach(e => {
        Array.from(e.attributes).forEach(a => {
          if (a.name.indexOf('epub:') == 0) {
            var newName = 'data-' + a.name.replace(':', '_');
            e.setAttribute(newName, a.value);
            e.removeAttribute(a.name);
          }
        });
      })
    }


    try {

      getTextPromise(findBlob('mimetype'))
        .then(mimetype => {
          return getTextPromise(findBlob('META-INF/container.xml'));
        })
        .then(container => {
          var parser = new DOMParser();
          var doc = parser.parseFromString(container.text, 'text/xml');
          var root = doc.querySelector('container>rootfiles>rootfile');
          var path = root.getAttribute('full-path');
          var rootDir = PATH.dirname(path);
          return getTextPromise(find(path).blob, rootDir);
        })
        .then((opfContent) => {
          var rootDir = opfContent.param;
          var parser = new DOMParser();
          var opf = parser.parseFromString(opfContent.text, 'text/xml');
          var meta = opf.querySelectorAll('package>metadata>*');
          var spine = opf.querySelectorAll('package>spine>itemref');
          var manifest = opf.querySelector('package>manifest');

          // Navigation and cover
          var nav = manifest.querySelector('item[properties="nav"]');
          var cover = manifest.querySelector('item[properties="cover-image"]');

          var xhtmls = [];
          Array.from(spine).forEach(ref => {
            var idref = ref.getAttribute('idref');
            var item = manifest.querySelector(`item[id="${idref}"]`);
            var href = PATH.join(rootDir, item.getAttribute('href'));
            var blob = findBlob(href);
            xhtmls.push(getTextPromise(blob, PATH.dirname(href)));
          });

          Promise.all(xhtmls).then(xhtmlsContent => {
            var body = [], css = [];
            var parser = new DOMParser();
            var coverImage, navDoc;

            // UNZIP does not provide a blob type. This isn't a problem wirth the exception of SVG.
            function convertBlob(filename, blob) {
              var suffix = filename.substr(filename.lastIndexOf('.') + 1).toLowerCase();
              return suffix == 'svg' ? new Blob([blob], { type: 'image/svg+xml' }) : blob;
            }

            if (cover) {
              var mediaType = cover.getAttribute('media-type');
              var path = PATH.join(rootDir, cover.getAttribute('href'));
              var file = find(path);
              if (file) {
                var blob = convertBlob(file.filename, file.blob);
                coverImage = document.createElement('img');
                coverImage.setAttribute('data-filename', PATH.basename(file.filename));
                coverImage.setAttribute('src', window.URL.createObjectURL(blob));
              };
            }

            xhtmlsContent.forEach(content => {
              var xhtml = parser.parseFromString(content.text, "text/xml");
              var title = xhtml.querySelectorAll('head>title');
              var links = xhtml.querySelectorAll('head>link[type*="text/css"]');
              var imgs = xhtml.querySelectorAll('body img');
              var images = xhtml.querySelectorAll('body image');
              var anchors = xhtml.querySelectorAll('body a[href]');
              var xhtmlDir = content.param;

              transformEpubNamespaceAttributes(xhtml);

              Array.from(imgs).forEach(img => {
                var path = PATH.join(xhtmlDir, img.getAttribute('src'));
                var file = find(path);
                if (file) {
                  var blob = convertBlob(file.filename, file.blob);
                  img.setAttribute('data-filename', PATH.basename(img.src));
                  img.setAttribute('src', window.URL.createObjectURL(blob));
                };
              });

              // Currently not possible to load an SVG image from a blob (???)
              Array.from(images).forEach(image => {
                var href = image.getAttribute('xlink:href');
                var path = PATH.join(xhtmlDir, href);
                var file = find(path);
                if (file) {
                  var blob = convertBlob(file.filename, file.blob);
                  image.setAttribute('data-filename', PATH.basename(href));
                  image.setAttributeNS('http://www.w3.org/1999/xlink', 'xlink:href', window.URL.createObjectURL(blob));
                };
              });

              // Adapt anchors
              Array.from(anchors).forEach(a => {
                var href = a.getAttribute('href');
                if (href.match(/^https?:\/\//i)) {
                  a.setAttribute('target', '_blank');
                }
                else {
                  var pos = href.indexOf('#');
                  if (pos > 0) {
                    href = href.substr(pos);
                    a.setAttribute('href', href);
                  };
                }
              })

              // Handle stylesheets
              Array.from(links).forEach(link => {
                var href = link.getAttribute('href');
                var path = PATH.join(xhtmlDir, href);
                css.push(getTextPromise(findBlob(path)));
              });
              var bdy = xhtml.querySelector('body');
              if (bdy) {
                body.push(bdy.innerHTML);
              }
            });

            Promise.all(css).then(cssContent => {

              var html = parser.parseFromString('', "text/html");

              // Process and add styles
              var allCSS = cssContent.map(e => { return e.text }).join('\n');
              var rules = processStyles(allCSS);
              var style = document.createElement('style');
              var content = [];
              for (var sel in rules) {
                content.push(transformEpubNamespaceSelectors(sel) + ' ' + rules[sel]);
              }
              style.textContent = content.join('\n');
              html.head.appendChild(style);

              // Add the meta data
              Array.from(meta).forEach(m => {
                var mel = document.createElement('meta');
                mel.setAttribute('name', m.nodeName.replace(/dc:/, 'DC.'));
                mel.setAttribute('content', m.textContent);
                html.head.appendChild(mel);
              });

              html.body.innerHTML = body.join('\n');
              if (coverImage) {
                html.body.insertBefore(coverImage, html.body.firstChild);
              }
              if (nav) {
                var href = PATH.join(rootDir, nav.getAttribute('href'));
                var blob = findBlob(href);
                getTextPromise(blob).then(nav => {
                  // console.log('NAV', nav);
                });
              }
              callback(html);
            });

          });

        })
    }

    catch(err) {
      console.error(err);
      alert(err);
    }

  });
}
