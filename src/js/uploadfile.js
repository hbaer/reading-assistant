/*
 * Upload a file.
 */

module.exports = function (options, content, mimetype) {

  const WSCLIENT = require('./ws-client.js');
  const NOTIFY = require('./notify.js');
  const SERVICE = 'ws://www.ursamedia.ch:8080/upload.js';
    
  console.log(options);
  
  var wsClient = WSCLIENT(SERVICE, status => {
    NOTIFY('File Upload', `Status code: ${status}`);
    wsClient.stop();
  });
  wsClient.start(() => {
    wsClient.send(JSON.stringify(options));
    var blob = new Blob([content], { type: mimetype });
    wsClient.send(blob);
  });

}
