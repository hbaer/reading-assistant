/*
 * Extract text from HTML element
 */

module.exports = function (element) {

  // Extract text
  function extractText(element) {
    var text = [];
    var node;
    var walk = document.createTreeWalker(element, NodeFilter.SHOW_TEXT | NodeFilter.SHOW_ELEMENT, null, false);
    while (node = walk.nextNode()) {
      // Avoid hidden text
      if (node.parentNode.offsetParent != null) {
        switch (node.nodeType) {
        // Transform line break to carriage return
        // case Node.ELEMENT_NODE:
        //   if (node.nodeName === 'BR') {
        //     text.push('\n');
        //   }
        //   break;
        case Node.TEXT_NODE:
          text.push(node.data);
          break;
        }
      }
    };
    return text;
  }
  
  return extractText(element);
}
