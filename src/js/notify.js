/*
 * Send a notification.
 */

module.exports = function notify(title, text, icon = 'static/bear.png') {

  if (!window.Notification) { return }
  
  if (Notification.permission === 'default') {
    Notification.requestPermission(function(result) {
      if (result === 'granted') {
        notify(title, text, icon);
      }
    })
  }
  else if (Notification.permission === 'granted') {
    var notification = new Notification(title, { body: text, icon: icon });
    setTimeout(notification.close.bind(notification), 4000);
    // notification.onclick = function() {
    //   alert('click');
    // }
  }
  else if (Notification.permission === 'denied') {
    console.log(title, text, Notification.permission);
  }

}
