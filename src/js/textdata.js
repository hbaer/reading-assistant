/*
 * Store text data.
 */

module.exports = function(text, name, type, date, dict, data) {

  var stringutils = require('./stringutils.js');

  var metaInfo = [{ name: 'document', content: name }];
  var accessor, changed = false;

  const SPLITS = /([.,;:…?¿!¡"'«»‹›‘’‚“”„(){}\[\]—<>≤≥=≠+×÷∞°#&%‰@§†©®•$£€/|\\]|\-+|\*+|\s+|_+)/g;

  // Tokeinze text
  function tokenize(text) {
    return text.split(SPLITS)
      .filter(w => { return w != '' })
      .map(w => { return { word: w } });
  };


  // Create a word list
  function createWordList(element) {
    var wordList = [];
    var node;
    var walk = document.createTreeWalker(element, NodeFilter.SHOW_TEXT, null, false);
    while (node = walk.nextNode()) {
      var words = tokenize(node.data);
      wordList = wordList.concat(words);
    };
    return wordList;
  }


  // Create a dictionary
  function createDictionary(elements) {
    var words = {}, pos = {};
    var dict = [];
    var p = 0;
    elements.forEach((e, i) => {
      var word = e.word;
      if (!words.hasOwnProperty(word)) {
        words[word] = [];
        pos[word] = [];
      }
      words[word].push(i);
      pos[word].push(p);
      p += word.length;
    });
    for (var word in words) {
      dict.push({ word: word, index: words[word], pos: pos[word] });
    }
    /*
    dict.sort((a, b) => {
      // return b['index'].length - a['index'].length;
      return a.word.localeCompare(b.word);
    });
    */
    return dict;
  }


  // Create sequence list
  function createSequenceList(text, sequence) {
    var list = [],
        index = 0,
        slen = sequence.length;
    while ((index = text.indexOf(sequence, index)) != -1) {
      list.push([index, index + slen]);
      index += slen;
    }
    return list;
  }

  // Create matched pattern list
  function createMatchedPatternList(text, pattern, flags='gu') {
    var re = new RegExp(pattern, flags);
    var match;
    var list = [];
    // non-capturing group
    while ((match = re.exec(text)) !== null) {
      if (match.length == 1) {
        list.push([match.index, re.lastIndex]);
      }
      // capturing group
      else {
        var e = match[1];
        if (e) {
          var p = match.index + match[0].indexOf(e);
          list.push([p, p + e.length]);
        }
      }
    }
    return list;
  }

  // Create element list
  function createElementList(elements, css) {
    return Array.from(elements.querySelectorAll(css));
  };

  // Create ngrams
  function createNGrams(elements, n, ignore) {
    var ngrams = {}, pos = {};
    var ngram = []
    var dict = [];
    var p = 0;
    elements.forEach((e, i) => {
      if (!e.word.match(SPLITS)) {
        ngram.push([e.word, i, p]);
        if (ngram.length >= n) {
          var sequence = ngram.map(a => { return a[0] }).join(' ');
          var key = ignore ? sequence.toLowerCase() : sequence;
          if (!ngrams.hasOwnProperty(key)) {
            ngrams[key] = [];
            pos[key] = [];
            if (ignore) { ngrams[key].o = sequence; }
          }
          var a = ngram[0];
          ngrams[key].push(a[1]);
          pos[key].push(a[2]);
          ngram.shift();
        }
      }
      p += e.word.length;
    });
    for (var word in ngrams) {
      dict.push({ word: ignore ? ngrams[word].o : word, index: ngrams[word], pos: pos[word] });
    }
    dict = dict.filter(word => {
      return word['index'].length >= 2;
    });
    /*
    dict.sort((a, b) => {
      return b['index'].length - a['index'].length;
    });
    */
    return dict;
  }


  // Create tag list
  function createTagList(element) {
    var walk = document.createTreeWalker(element, NodeFilter.SHOW_ELEMENT, null, false);
    var node = walk.nextNode(),
        index = [],
        id = 0;
    while (node = walk.nextNode()) {
      node.classList.add('ra-hl');
      node.setAttribute('data-id', id);
      // var start = (node.firstChild && node.firstChild.data) ? stringutils.limitString(node.firstChild.data) : '???';
      // var text = `${node.nodeName} | ${start}`;
      var text = node.outerHTML;
      index.push({ word: text, id: id++, index: [] });
    };
    return index;
  }


  return {
    getText: function() {
      return text;
    },
    getDict: function() {
      return dict;
    },
    getData: function() {
      return data;
    },
    setTextAccessor: function(_) {
      accessor = _;
      text = null;
    },
    getTextAccessor: function() {
      return accessor;
    },
    getRawText: function() {
      return accessor().textContent;
    },
    getType: function() {
      return type;
    },
    getFileName: function() {
      return unescape(name);
    },
    setFileName: function(_) {
      name = _;
    },
    setMetaInfo: function(info) {
      metaInfo = info;
    },
    getMetaInfo: function() {
      return metaInfo;
    },
    addMetaInfo: function(entry) {
      metaInfo = metaInfo || [];
      metaInfo.push(entry);
    },
    replaceMetaInfo: function(entry) {
      if (metaInfo) {
        metaInfo = metaInfo.filter(e => {
          return e.name !== entry.name;
        });
      }
      this.addMetaInfo(entry);
    },
    set changed(_) {
      changed = _;
    },
    get changed() {
      return changed;
    },
    createWordList: function() {
      return createWordList(accessor());
    },
    createDictionary: function() {
      return createDictionary(createWordList(accessor()));
    },
    createNGrams: function(n = 3, ignore = true) {
      return createNGrams(createWordList(accessor()), n, ignore);
    },
    createTagList: function() {
      return createTagList(accessor());
    },
    createSequenceList: function(pattern) {
      return createSequenceList(accessor().textContent, pattern);
    },
    createMatchedPatternList: function(pattern, flags) {
      return createMatchedPatternList(accessor().textContent, pattern, flags);
    },
    createElementList: function(pattern) {
      return createElementList(accessor(), pattern);
    }
  }

}
