/*
 * Get content from URL
 */

module.exports = function (url, callback, notify) {

  const WSCLIENT = require('./ws-client.js');
  const NOTIFY = require('./notify.js');
  const PATH = require('path-browserify');
  const SERVICE = 'ws://www.ursamedia.ch:8080/relais.js';
  
  var newLocation, contentType;

  var wsClient = WSCLIENT(SERVICE, data => {
    if (notify === false) {
      NOTIFY('Cross-Origin Resource Sharing', 'By way of "www.ursamedia.ch"');
    };
    if (typeof(data) === 'string') {
      var headers = JSON.parse(data);
      contentType = headers['content-type'];
      if (headers.location) {
        newLocation = headers.location;
        console.log('Address relocated.');
      }
    }
    else {
      if (newLocation) {
        url = newLocation;
        newLocation = null;
        wsClient.send(url);
      }
      else {
        var basename = PATH.basename(url);
        var blob = new Blob([data], { type: contentType });
        blob.name = basename;
        callback(blob);
        wsClient.stop();
      }
    }
  });
  wsClient.start(() => {
    wsClient.send(url);
  });

}
