/*
 * Get a small but sufficiently unique identifier.
 */

module.exports = function() {

  const BASE = 36;
  const MAX = Math.pow(BASE, 8);
  var start = Date.now();
  var counter = 0;

  return function() {
    return ((start + counter++) % MAX).toString(BASE);
  }

}
