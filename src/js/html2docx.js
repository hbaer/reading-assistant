/*
 * Create DOCX from HTML
 */

module.exports = function (htmlString) {

  var htmlDocx = require('html-docx-js/dist/html-docx.js');
  var docx = htmlDocx.asBlob(htmlString);

  return docx;
}
