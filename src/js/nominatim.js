/*
 * N O M I N A T I M
 * Reverse geocoding
 */

 var d3 = require('d3-request');

 function request(url, callback) {
   d3.request(url)
   .responseType('json')
   .get((error, xhr) => {
     if (error) {
       console.error(error);
       alert(`Could not get "${url}"`);
     }
     else {
       callback(xhr.response);
     }
   })
 }

 function createQueryString(query) {
   return Object.keys(query).map(key => {
     return key + '=' + query[key];
   }).join('&');
 }

 function getURL(method, params) {
   var domain = 'https://nominatim.openstreetmap.org';

   var query = {
     format: 'json',
     'polygon_geojson': 1,
     'accept-language': 'en'
   };

   var qs = createQueryString(params) + '&' + createQueryString(query);
   var url = `${domain}/${method}?${qs}`;
   return url;
 }

 function run(method, params, callback) {
   var url = getURL(method, params);
   request(url, place => {
     callback(place);
   });
 }

module.exports = {
  search: function(params, callback) {
    run('search', params, callback);
  },
  reverse: function(params, callback) {
    run('reverse', params, callback);
  }
}
