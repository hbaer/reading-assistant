/*
 * Create zipped HTML file from node list.
 */

module.exports = function (element, index, data, meta, styles, filename, callback) {

  const SERIALIZEHTML = require('./serializehtml.js');
  const JSZIP = require('jszip');

  var copy = element.cloneNode(true);

  var zip = new JSZIP();

  function getImagePromise(blob, name) {
    return new Promise((resolve, reject) => {
      var xhr = new XMLHttpRequest();
      xhr.open('GET', blob, true);
      xhr.responseType = 'arraybuffer';
      xhr.onload = function(e) {
        resolve({ data: this.response, name: name });
      }
      xhr.send();
    });
  }
  
  function generateZip() {
    zip.generateAsync({ type: 'blob', compression: 'DEFLATE' })
      .then(function(content) {
        callback(content);
      });
  }


  var promises = [];
  var imgs = copy.querySelectorAll('img');
  Array.from(imgs).forEach(img => {
    var name = img.getAttribute('data-filename');
    promises.push(getImagePromise(img.src, name));
    img.setAttribute('src', 'images/' + name);
    img.removeAttribute('data-filename');
  });
  var images = copy.querySelectorAll('image');
  Array.from(images).forEach(image => {
    var name = image.getAttribute('data-filename');
    var src = image.getAttribute('xlink:href');
    promises.push(getImagePromise(src, name));
    image.setAttribute('xlink:href', 'images/' + name);
    image.removeAttribute('data-filename');
  });

  var htmlString = SERIALIZEHTML(copy, meta, styles);
  zip.file('text.html', htmlString);

  if (index) {
    var htmlString = SERIALIZEHTML(index.cloneNode(true));
    zip.file('dict.html', htmlString);
  }

  zip.file('data.json', JSON.stringify(data, null, 2))
  // generateZip(); // WITHOUT IMAGES!!!

  Promise.all(promises).then(images => {

    if (images.length > 0) {
      var img = zip.folder('images');
      Array.from(images).forEach(imgData => {
        img.file(imgData.name, imgData.data);
      })
    };
    
    generateZip();

  }).catch(reason => { 
    console.log(reason);
    generateZip();
  });

}
