/*
 * Access files in the cloud.
 */

module.exports = function (options, callback, blob) {

  const WSCLIENT = require('./ws-client.js');
  const NOTIFY = require('./notify.js');
  const PATH = require('path-browserify');
  const SERVICE = 'ws://www.ursamedia.ch:8080/cloud.js';
  
  var contentType, authenticate, data = [];

  var wsClient = WSCLIENT(SERVICE, d => {
    if (typeof(d) === 'string') {
      if (d.indexOf('status') == 0) {
        
        var status = +d.substr(d.indexOf(':') + 1);
        if (status == 401) {
          var method = authenticate.split(' ', 1)[0];
          console.log(authenticate);
          var params = {};
          authenticate.substr(method.length + 1).split(',').map(e => {
            return e.split(/=(.+)/);
          }).forEach(e => {
            params[e[0]] = e[1].replace(/^\"|\"$/g, "");
          });
          console.log(params);
        }
        
        NOTIFY('HTTP Request', `Status code: ${d}`);
        var basename = PATH.basename(options.path);
        var blob = new Blob(data, { type: contentType });
        blob.name = basename;
        if (callback) {
          callback(blob);
        }
        wsClient.stop();
      }
      else {
        var headers = JSON.parse(d);
        contentType = headers['content-type'];
        authenticate = headers['www-authenticate'];
      }
    }
    else {
      data.push(d);
    }
  });
  wsClient.start(() => {
    wsClient.send(JSON.stringify(options));
    if (blob) {
      wsClient.send(blob);
    }
    wsClient.send(JSON.stringify({ end: true }));
  });

}
