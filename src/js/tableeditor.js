/*
 * Table Editor.
 */

module.exports = function(table, row, callback, display) {

  table = typeof(table) == 'string' ? document.querySelector(table) : table;
  var tableData = [],
      selecting,
      currentIndex = -1;

  display.setData(tableData);

  // Gets the parent element with given class
  function getParentWithClass(element, cls = 'row') {
    while (element) {
      if (element.classList && element.classList.contains(cls)) {
        currentIndex = getIndex(element);
        break;
      }
      element = element.parentElement;
    }
    return element;
  }

  // Select the contents of an element
  function selectElementContents(el) {
    el.focus();
    var range = document.createRange();
    range.selectNodeContents(el);
    var sel = window.getSelection();
    sel.removeAllRanges();
    sel.addRange(range);
    getParentWithClass(sel.anchorNode); // Just to keep track of the current element.
  }

  // Gets the index for a DOM element
  function getIndex(el) {
    return tableData.indexOf(el);
  }

  // Gets the currently selected row
  function getCurrentRow() {
    var selection = document.getSelection();
    return getParentWithClass(selection.anchorNode);
  }

  // Handle key down events
  table.addEventListener('keydown', evt => {
    var element;
    switch (evt.key) {
    case 'Enter':
      if (evt.shiftKey == false) {
        element = insertRow(currentIndex);
        selectElementContents(element.firstChild);
        callback({ cmd: 'add', row: element, evt })
      }
      else {
        document.execCommand('insertHTML', false, '\n\r');
      }
      evt.preventDefault();
      break;
    case 'Backspace':
      element = tableData[currentIndex];
      if (element) {
        if (element.firstChild.textContent.length == 0) {
          selectElementContents(element.firstChild);
          var el = removeRow();
          if (el) {
            selectElementContents(el.firstChild);
          }
          callback({ cmd: 'rm', row: element, evt });
          evt.preventDefault();
        }
      }
      break;
    }
  });

  // Handle key up events
  table.addEventListener('keyup', evt => {
    var selection = document.getSelection();
    switch (evt.key) {
    case 'ArrowLeft':
    case 'ArrowRight':
    case 'ArrowUp':
    case 'ArrowDown':
      var row = getParentWithClass(evt.target, 'row');
      callback({ cmd: 'nav', row, key: evt.key, evt });
      break;
    }
  });

  // Handle clicks
  table.addEventListener('click', evt => {
    var row = getParentWithClass(evt.target, 'row');
    callback({ cmd: 'click', row, cell: evt.target, evt, pos: [currentIndex + 1, tableData.length] });
  });

  // Handle double-clicks
  table.addEventListener('dblclick', evt => {
    var row = getParentWithClass(evt.target, 'row');
    callback({ cmd: 'dblclick', row, cell: evt.target, evt });
  });

  // Handle mouse down
  table.addEventListener('mousedown', evt => {
    selecting = true;
    var row = getParentWithClass(evt.target, 'row');
    callback({ cmd: 'selstart', row, cell: evt.target, evt });
  });

  // Handle mouse up
  table.addEventListener('mouseup', evt => {
    selecting = false;
    var row = getParentWithClass(evt.target, 'row');
    callback({ cmd: 'selend', row, cell: evt.target, evt });
  });

  // Handle mouse over
  table.addEventListener('mouseover', evt => {
    if (selecting) {
      var row = getParentWithClass(evt.target, 'row');
      callback({ cmd: 'seladd', row, cell: evt.target, evt });
    }
  });

  // Handle paste events
  table.addEventListener('paste', evt => {
    var content = evt.clipboardData.getData('text');
    insertData(content);
    evt.preventDefault();
  });

  function addRow(data) {
    var element = row(...data);
    currentIndex = tableData.push(element) - 1;
    display.update();
    selectElementContents(element.firstChild);
    return element;
  }

  function forEachRow(callback) {
    tableData.forEach(row => {
      callback(row);
    })
  }

  function forEachVisibleRow(callback) {
    var visibleData = display.getData()
    visibleData.forEach(row => {
      callback(row);
    })
  }

  function filterRows(callback) {
    display.removeAllRows();
    tableData = tableData.filter(row => {
      return !callback(row);
    });
    display.setData(tableData);
  }

  function insertRow(index, fields) {
    var element = fields == null ? row() : row(...fields);
    tableData.splice(++index, 0, element);
    currentIndex = index;
    display.reset();
    display.scrollToIndex(index);
    return element;
  }

  function removeRow(element) {
    if (element) {
      currentIndex = getIndex(element);
    }
    tableData.splice(currentIndex, 1);
    display.reset();
    currentIndex = Math.min(tableData.length - 1, currentIndex);
    return tableData[currentIndex];
  }

  function removeAllRows() {
    display.removeAllRows();
    tableData = [];
    display.setData(tableData);
  }

  function insertData(data) {
    var elements = [];
    var rows = data.split(/(?:\r\r|\n\n|\r\n\r\n)/g);
    rows.forEach(r => {
      if (r.length > 0 && r.charAt(0) != '#') {
        r = r.replace(/\r\n?|\n/g, '\n');
        var fields = r.split(/\t/g);
        var element = row(...fields);
        elements.push(element);
      }
    });
    tableData.splice(currentIndex + 1, 0, ...elements);
    display.reset();
    display.scrollToIndex(currentIndex);
  }

  function getDataObject(callback) {
    return tableData.map(d => {
      return callback(d);
    })
  }

  return {
    addRow: function(...data) {
      var element = row(...data);
      currentIndex = tableData.push(element) - 1;
      return element;
    },
    insertRow: function(...data) {
      return insertRow(currentIndex, data);
    },
    getCurrentRow: function() {
      return tableData[currentIndex];
    },
    forEachRow: function(callback) {
      forEachRow(callback);
    },
    forEachVisibleRow: function(callback) {
      forEachVisibleRow(callback);
    },
    filterRows: function(callback) {
      filterRows(callback);
    },
    removeRow: function(row) {
      return removeRow(row);
    },
    removeAllRows: function() {
      removeAllRows();
    },
    insertData: function(data) {
      insertData(data);
    },
    setData: function(_) {
      tableData = _;
      display.setData(_);
    },
    getData: function() {
      return tableData;
    },
    getDataObject(columns) {
      return getDataObject(columns);
    },
    mapData: function(data) {
      var x = data.map((d, i) => { return tableData[d[1]] });
      this.removeAllRows();
      this.setData(x);
      display.update();
      display.scrollToIndex(0);
    },
    getColumnValue: function(accessor) {
      return tableData.map((d, i) => { return [ accessor(d), i ] });
    },
    filterTable: function(callback) {
      display.removeAllRows();
      if (callback) {
        var rows = tableData.filter(row => {
          return callback(row);
        })
        display.setData(rows);
      }
      else {
        display.setData(tableData);
      }
      display.update();
      display.scrollToIndex(0);
    },
    update: function() {
      display.reset();
      if (tableData.length > 0) {
        currentIndex = Math.min(currentIndex, tableData.length - 1);
        selectElementContents(tableData[currentIndex].firstChild);
      }
    },
    scrollToIndex: function(index) {
      display.scrollToIndex(currentIndex = index);
    },
    scrollToElement: function(row) {
      var index = tableData.indexOf(row);
      if (index >= 0) {
        this.scrollToIndex(index);
      }
    }
  }

}
