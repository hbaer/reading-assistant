/*
 * Create HTML file from node tree.
 */

module.exports = function (element, meta, styles) {

  const pretty = require('pretty');
  const DOCTYPE = '<!doctype html>';

  var parser = new DOMParser();
  var htmlDoc = parser.parseFromString(DOCTYPE, 'text/html');
  var node = element.cloneNode(true);
  Array.from(node.childNodes).forEach(child => {
    htmlDoc.body.appendChild(child);
  })

  // Add character set meta information
  var charset = document.createElement('meta');
  charset.setAttribute('charset', 'UTF-8');
  htmlDoc.head.appendChild(charset);

  if (meta) {
    var creator, title, lang;
    Array.from(meta).forEach(m => {
      var entry = document.createElement('meta');
      entry.setAttribute('name', m.name);
      entry.setAttribute('content', m.content);
      htmlDoc.head.appendChild(entry);

      if (m.name.match(/creator$/i)) {
        creator = m.content;
      }
      else if (m.name.match(/title$/i)) {
        title = m.content;
      }
      else if (m.name.match(/language$/i)) {
        lang = m.content;
      }
    });

    if (creator && title) {
      var titleEl = document.createElement('title');
      titleEl.textContent = creator + ': ' + title;
      htmlDoc.head.appendChild(titleEl);
    }

    if (lang) {
      htmlDoc.documentElement.setAttribute('lang', lang);
    }
  }

  if (styles) {
    styles.forEach(css => {
      var style;
      if (typeof css.content == 'string') {
        style = document.createElement('style');
        style.textContent = css.content;
      }
      else {
        style = css.content.cloneNode(true);
      }
      if (css.cls) {
        style.classList.add(css.cls);
      }
      htmlDoc.head.appendChild(style);
    });
  }

  // Restore the original file name for images
  /*
  var imgs = htmlDoc.body.querySelectorAll('img');
  Array.from(imgs).forEach(img => {
    var name = img.getAttribute('data-filename');
    img.setAttribute('src', name);
    img.removeAttribute('data-filename');
  });
  var images = htmlDoc.body.querySelectorAll('image');
  Array.from(images).forEach(image => {
    var name = image.getAttribute('data-filename');
    var src = image.getAttribute('xlink:href');
    image.setAttribute('xlink:href', name);
    image.removeAttribute('data-filename');
  });
  */

  var serializer = new XMLSerializer();
  var htmlString = serializer.serializeToString(htmlDoc);

  return pretty(htmlString, { ocd: true });
}
