/*
 * Get HTML from a TEI file.
 */

module.exports = function (data, callback) {

  function getNode(node, query) {
    return node.querySelector(query);
  };

  function getNodes(node, query) {
    return node.querySelectorAll(query);
  };

  function appendMeta(html, elements) {
    Array.from(elements).forEach(element => {
      var meta = document.createElement('meta');
      meta.setAttribute('name', 'DC.' + element.nodeName);
      meta.setAttribute('content', element.textContent);
      html.head.appendChild(meta);
    });
  }

  function handleHeader(html, header) {
    appendMeta(html, getNodes(header, 'fileDesc>titleStmt>*'));
    appendMeta(html, getNodes(header, 'fileDesc>publicationStmt>*'));
    appendMeta(html, getNodes(header, 'fileDesc>notesStmt>*'));
    appendMeta(html, getNodes(header, 'fileDesc>sourceDesc>*'));
  };

  function handleText(header, text) {
    var parser = new DOMParser();
    var html = parser.parseFromString('', 'text/html');

    handleHeader(html, header);

    var node,
        walk = document.createTreeWalker(text, NodeFilter.SHOW_ELEMENT, null, false);

    while (node = walk.nextNode()) {

      switch(node.nodeName) {

        case 'p':
        var p = document.createElement('p');
        p.textContent = node.textContent;
        html.body.appendChild(p);
        break;

        case 'head':
        var type = node.getAttribute('type');
        var h = document.createElement(type);
        h.textContent = node.textContent;
        html.body.appendChild(h);
        break;

      }
    };
    callback(html);
  };

  function handleTEIs(teis) {
    teis.forEach(tei => {
      var header = getNode(tei, 'teiHeader');
      var text = getNode(tei, 'text');
      handleText(header, text);
    });
  };

  function handleRootElement(root) {
    var corpus = getNode(root, 'teiCorpus');
    if (corpus) {
      // showContent(getNode(corpus, 'teiHeader'));
      var tei = Array.from(getNodes(corpus, 'TEI'));
      handleTEIs(tei);
    }
    else {
      handleTEIs([root]);
    }
  };

  var parser = new DOMParser();
  var xml = parser.parseFromString(data, 'text/xml');
  var root = xml.documentElement;

  handleRootElement(root);

}
