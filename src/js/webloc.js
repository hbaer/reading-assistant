/*
 * Get content from a webloc file
 */

module.exports = function (file, callback) {

  var reader = new FileReader();
  reader.onloadend = function(e) {
    var domParser = new DOMParser();
    var dom = domParser.parseFromString(this.result, 'text/xml');
    var url = dom.documentElement.querySelector('dict>string').textContent;
    callback(url);
  }
  reader.readAsText(file);
  
}
