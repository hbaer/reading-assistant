/*
 * Get text and meta data from a PDF
 */

module.exports = function (url, callback) {

  require('pdfjs-dist/build/pdf');
  PDFJS.workerSrc = require('pdfjs-dist/build/pdf.worker.js');
  
  function formatMeta(meta) {
    return Object.entries(meta).map(e => { return { name: e[0], content: e[1] } });
  }

  function getText(pdfUrl){

    var pdf = PDFJS.getDocument(pdfUrl);

    return pdf.then(function(pdf) {
      var maxPages = pdf.pdfInfo.numPages;
      var countPromises = [];

      for (var j = 1; j <= maxPages; j++) {
        var page = pdf.getPage(j);
        var txt = '';

        countPromises.push(page.then(function(page) {
          var textContent = page.getTextContent();
          return textContent.then(function(text) {
            
            return text.items.map(function(s, i, a) {
              // Format text according to text item positions
              var t = '';
              if (i > 0) {
                var r = a[i - 1];
                var [x1, y1] = [r.transform[4], r.transform[5]];
                var [x2, y2] = [s.transform[4], s.transform[5]];
                                
                if (y1 != y2) {
                  t = '\n'.repeat(Math.max(1, Math.min(3, Math.round(Math.abs(y2 - y1) / s.height))));
                }
                else if (x1 + r.width + 0.2 * r.height < x2) {
                  t = ' ';
                }
              }
              return t + s.str;
            }).join('');
          });
          
        }));
      };
      
      countPromises.push(pdf.getMetadata());
      
      // Wait for all tasks, format meta data and join text.
      return Promise.all(countPromises).then(function(texts) {
        var meta = texts.pop();
        return { meta: formatMeta(meta.info), text: texts.join('\n\n') };
      });

    });
  }

  getText(url).then(function (text) {
    callback(text);
  }, function (reason) {
    console.error(reason);
  });

}

