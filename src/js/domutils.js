/*
 * DOM Utilities.
 */

module.exports = {

  // Open document in a new window and send data to new window
  openWithData: function(file, data) {
    var win = window.open(file, '_blank', 'menubar=no,toolbar=no,location=no,scrollbars=yes');
    win.addEventListener('load', evt => {
      win.postMessage(data, '*');
    });
  },

  // Open a window with data URL
  openDocument: function(doc, type) {
    var content = `data:${type},${encodeURIComponent(doc)}`;
    window.open(content, '_blank', 'menubar=no,toolbar=no,location=no,scrollbars=yes');
  },

  // Get content of head element
  contentOfHead: function(doc) {
    return doc.split(/<\/?head.*?>/i)[1];
  },

 // Get content of body element
  contentOfBody: function(doc) {
    return doc.split(/<\/?body.*?>/i)[1];
  },

  // Collect content of meta elements
  createMetaInfo: function(doc) {
    var meta = doc.querySelectorAll('head>meta');
    var metaInfo = [];
    Array.from(meta).forEach(node => {
      var name = node.getAttribute('name');
      if (name) {
        var content = node.getAttribute('content');
        metaInfo.push({ name, content });
      }
    });
    return metaInfo;
  },

  // Collect content of meta elements from HTML text
  createMetaInfoFromString: function(content) {
    var parser = new DOMParser();
    var doc = parser.parseFromString(content, 'text/html');
    return this.createMetaInfo(doc);
  },

  // Get content of style elements from HTML text
  getStyleContent: function(content) {
    var parser = new DOMParser();
    var doc = parser.parseFromString(content, 'text/html');
    var style = doc.querySelector('head>style');
    return style ? style.textContent : '';
  },

  // Remove all children from an element
  removeChildren: function(where) {
    while (where.firstChild) {
      where.removeChild(where.firstChild);
    }
  },

  // Add a list of elements to a previously emptied element
  fillWithNodeList: function(where, list) {
    removeChildren(where);
    for (var node of list) {
      where.appendChild(node);
    }
  },

  // Return the computed style of an element
  getComputedStyle: function(element, property) {
    return window.getComputedStyle(element).getPropertyValue(property);
  },

  // Return the field of a computed style
  getComputedStyleField: function(element, property, index) {
    return this.getComputedStyle(element, property).split(' ')[index];
  },

  // Remove the parent selector from CSS text
  removeParentSelector: function(cssText, parent) {
    function escapeRegExp(str) {
      return str.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"); // $& means the whole matched string
    }
    var re = new RegExp(escapeRegExp(parent), 'g');
    return cssText.replace(re, '');
  },

  // Call function for every element
  walkElementTree: function(element, callback, filter = NodeFilter.SHOW_ELEMENT) {
    var walk = document.createTreeWalker(element, NodeFilter.SHOW_ELEMENT, null, false),
        node,
        i = 0;
    while (node = walk.nextNode()) {
      callback(node, i++);
    };
  },

  // Create selector List
  selectorList: function() {
    var selectors = new Set();
    return {
      addElement: function(element) {
        selectors.add(element.nodeName);
        if (element.id) {
          selectors.add('#' + element.id);
        }
        element.classList.forEach(e => {
          selectors.add('.' + e);
        })
      },
      getList: function() {
        return selectors;
      }
    };
  }

}
