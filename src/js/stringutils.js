/*
 * String Utilities.
 */

module.exports = {
  
  /*
   * Limits the length of a text string.
   * Breaks at space
   * Appends ellipsis
   */
  limitString: function(str, max = 80, lim = 60) {
    return str.length > max ? str.substr(0, str.indexOf(' ', lim)) + '…' : str;
  }
}
