/*
 * Download a file.
 */

module.exports = function (filename, data, mimetype) {

  mimetype = mimetype || 'text/plain';

  var a = document.createElement('a');
  document.body.appendChild(a);
  var blob = new Blob([data], { 'type': mimetype });
  if (a.download !== undefined) {
    a.href = window.URL.createObjectURL(blob);
    a.download = filename;
    a.click();
    document.body.appendChild(a);
  }
  else {
    var content;
    if (window.URL) {
      var content = URL.createObjectURL(blob);
    }
    else {
      content = 'data:' + mimetype + ',' + encodeURIComponent(data);
    }
    window.open(content, '_blank', 'toolbar=0,location=0,menubar=0');
  }
  document.body.removeChild(a);
}
