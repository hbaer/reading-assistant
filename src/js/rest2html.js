/*
 * Get HTML from a reStructuredText.
 *
 * Depends on "structured", an imcomplete implementation.
 */

module.exports = function (data, callback) {

  const rst2html = require('rst2html');
  var content = rst2html(data);

  var parser = new DOMParser();
  var html = parser.parseFromString('', "text/html");
  html.body.innerHTML = content;
  
  
  var meta = document.createElement('meta');
  meta.setAttribute('name', 'Origin');
  meta.setAttribute('content', 'Converted from "reStructuredText"');
  html.head.appendChild(meta);
  
  callback(html);
}
