/*
 * Geo Utilities.
 */

module.exports = {

  // Counts the number of elements in nested array
  countElements: function(a) {
    var n = 0;
    function count(a) {
      if (Array.isArray(a)) {
        a.forEach(e => { count(e) });
      }
      else {
        n += 1;
      }
    }
    count(a);
    return n;
  }

}
