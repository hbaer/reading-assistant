/*
 * Get content from webarchive file
 */

module.exports = function (data, callback) {

  const parseplist = require('./parseplist.js');
  const parsebplist = require('./parsebplist.js');

  const enc = new TextDecoder();

  function pListType(data) {
    return enc.decode(data.slice(0, 6));
  }

  var obj;
  var type = pListType(data);
  if (type === 'bplist') {
    obj = parsebplist(new Buffer(data));
  }
  else {
    var text = enc.decode(data);
    var parser = new DOMParser();
    var doc = parser.parseFromString(text, 'text/xml');
    var type = doc.documentElement.nodeName;
    if (type === 'plist') {
      obj = parseplist(doc);
    }
  }

  var main = obj.WebMainResource;

  var parser = new DOMParser();
  var html = parser.parseFromString(main.WebResourceData.toString(), main.WebResourceMIMEType);

  var findImage = function() {
    var imgs = Array.from(html.body.querySelectorAll('img'));
    return function(name) {
      return imgs.find(img => {
        var src = img.getAttribute('src');
        return name.endsWith(src)
      })
    }
  }();

  var findLink = function() {
    var links = Array.from(html.head.querySelectorAll('link[rel=stylesheet]'));
    return function(name) {
      return links.find(link => {
        var href = link.getAttribute('href');
        return name.endsWith(href)
      })
    }
  }();

  obj.WebSubresources.forEach(res => {
    switch (res.WebResourceMIMEType) {
      case 'text/css':
      var link = findLink(res.WebResourceURL);
      if (link) {
        var style = document.createElement('style');
        style.textContent = res.WebResourceData.toString();
        html.head.appendChild(style);
      }
      break;
      case 'image/jpeg':
      case 'image/png':
      case 'image/gif':
      case 'image/svg+xml':
      if (res.WebResourceURL.indexOf('data:') != 0) {
        var img = findImage(res.WebResourceURL);
        if (img) {
          img.setAttribute('src', window.URL.createObjectURL(new Blob([res.WebResourceData], { type: res.WebResourceMIMEType })));
        }
        else {
          // console.log('Image not found', res.WebResourceMIMEType, res.WebResourceURL);
        }
      }
      break;
      default:
      console.log('Unhandled resource type:', res.WebResourceMIMEType /* , res.WebResourceData.toString() */);
    }
  });

  callback(html);

}
