/*
 * Drag & Drop Files
 */

module.exports = function(selector, passFiles) {

  var dropFiles = document.querySelector(selector);
  dropFiles.focus();

  dropFiles.addEventListener('dragover', function(e) {
    e.stopPropagation();
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy';
  });

  dropFiles.addEventListener('dragenter', function(e) {
    e.stopPropagation();
    e.preventDefault();
    dropFiles.classList.add('hilite');
  });

  dropFiles.addEventListener('dragleave', function(e) {
    e.stopPropagation();
    e.preventDefault();
    dropFiles.classList.remove('hilite');
  });

  dropFiles.addEventListener('paste', function(e) {
    if (e.clipboardData.types.indexOf('text/plain') > -1) {
      var content = e.clipboardData.getData('text/plain');
      passFiles(content);
      e.preventDefault();
    }
  });

  dropFiles.addEventListener('drop', function(e) {
    e.stopPropagation();
    e.preventDefault();
    dropFiles.classList.remove('hilite');
    var files = e.dataTransfer.files;
    var items = e.dataTransfer.items;
    if (files.length > 0) {
      passFiles(files);
    }
    else if (items && items.length > 0) {
      var entries = Array.from(items).map(item => {
        return item.webkitGetAsEntry();
      }).filter(item => {
        return item != null;
      });
      var dp = directoryParser(passFiles);
      dp(entries);
    }

    // console.log(e.dataTransfer.types, e.dataTransfer.types.length);

    var content = e.dataTransfer.getData('text/html');
    if (content) {
      var parser = new DOMParser();
      var doc = parser.parseFromString(content, 'text/html');
      passFiles(doc);
    }
    else {
      content = e.dataTransfer.getData('text/plain');
      if (content) {
        passFiles(content);
      }
    }
  });

  function directoryParser(callback) {
    function parseDirectory(entries) {
      entries.forEach(entry => {
        if (entry.isFile) {
          entry.file(file => {
            callback([file]);
          });
        }
        else if (entry.isDirectory) {
          var dirReader = entry.createReader();
          dirReader.readEntries(entries => {
            parseDirectory(entries);
          });
        }
      });
    };
    return function(entries) {
      parseDirectory(entries);
    }
  }

}
