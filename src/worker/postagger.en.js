onmessage = function(e) {

  const CODES = {
    'CC':   'Coordinating conjunction',
    'CD':   'Cardinal number',
    'DT':   'Determiner',
    'EX':   'Existential there',
    'FW':   'Foreign Word',
    'IN':   'Preposition',
    'JJ':   'Adjective',
    'JJR':  'Adjective, comparative',
    'JJS':  'Adjective, superlative',
    'LS':   'List item marker',
    'MD':   'Modal',
    'NN':   'Noun, singular or mass',
    'NNP':  'Proper noun, singular',
    'NNPS': 'Proper noun, plural',
    'NNS':  'Noun, plural',
    'POS':  'Possessive ending',
    'PDT':  'Predeterminer',
    'PP$':  'Possessive pronoun',
    'PRP':  'Personal pronoun',
    'RB':   'Adverb',
    'RBR':  'Adverb, comparative',
    'RBS':  'Adverb, superlative',
    'RP':   'Particle',
    'SYM':  'Symbol',
    'TO':   'Infinitive marker "to"',
    'UH':   'Interjection',
    'VB':   'Verb, base form',
    'VBD':  'Verb, past tense',
    'VBG':  'Verb, gerund',
    'VBN':  'Verb, past part',
    'VBP':  'Verb, present',
    'VBZ':  'Verb, present',
    'WDT':  'Wh determiner',
    'WP':   'Wh pronoun',
    'WP$':  'Possessive-Wh',
    'WRB':  'Wh adverb',
    ',':    'Comma',
    '.':    'Sentence final punctuation',
    ':':    'Mid-sentence punctuation',
    '$':    'Dollar sign',
    '#':    'Pound sign',
    "'":    'Quote',
    '(':    'Left parenthesis',
    ')':    'Right parenthesis'
  };

  const SPLITS = /([.,;:…?¿!¡"'«»‹›‘’‚“”„(){}\[\]—<>≤≥=≠+×÷∞°#&%‰@§†©®•$£€/|\\]|\-+|\*+|\s+|_+)/g;
  const SPACES = /^\s+$/;


  function BrillTransformationRules() {
    var rules = [rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8];
    //  rule 1: DT, {VBD | VBP} --> DT, NN
    function rule1(taggedSentence, index) {
      if ((index > 0) && (taggedSentence[index - 1][1] === "DT")) {
        if ((taggedSentence[index][1] === "VBD") ||
          (taggedSentence[index][1] === "VBP") ||
          (taggedSentence[index][1] === "VB")) {
          taggedSentence[index][1] = "NN";
        }
      }
    }

    // rule 2: convert a noun to a number (CD) if "." appears in the word
    function rule2(taggedSentence, index) {
      if (startsWith(taggedSentence[index][1], "N")) {
        if (taggedSentence[index][0].indexOf(".") > -1) {
          // url if there are two contiguous alpha characters
          if (/[a-zA-Z]{2}/.test(taggedSentence[index][0])) {
            taggedSentence[index][1] = "URL";
          }
          else {
            taggedSentence[index][1] = "CD";
          }
        }
        // Attempt to convert into a number
        if (!isNaN(parseFloat(taggedSentence[index][0]))) {
          taggedSentence[index][1] = "CD";
        }
      }
    }

    // rule 3: convert a noun to a past participle if words[i] ends with "ed"
    function rule3(taggedSentence, index) {
      if (startsWith(taggedSentence[index][1], "N") && endsWith(taggedSentence[index][0], "ed")) {
        taggedSentence[index][1] = "VBN";
      }
    }

    // rule 4: convert any type to adverb if it ends in "ly";
    function rule4(taggedSentence, index) {
      if (endsWith(taggedSentence[index][0], "ly")) {
        taggedSentence[index][1] = "RB";
      }
    }

    // rule 5: convert a common noun (NN or NNS) to a adjective if it ends with "al"
    function rule5(taggedSentence, index) {
      if (startsWith(taggedSentence[index][1], "NN") && endsWith(taggedSentence[index][0], "al")) {
        taggedSentence[index][1] = "JJ";
      }
    }

    // rule 6: convert a noun to a verb if the preceding work is "would"
    function rule6(taggedSentence, index) {
      if ((index > 0) && startsWith(taggedSentence[index][1], "NN") && (taggedSentence[index - 1][0].toLowerCase() === "would")) {
        taggedSentence[index][1] = "VB";
      }
    }

    // rule 7: if a word has been categorized as a common noun and it ends with "s",
    //         then set its type to plural common noun (NNS)
    function rule7(taggedSentence, index) {
      if ((taggedSentence[index][1] === "NN") && (endsWith(taggedSentence[index][0], "s"))) {
        taggedSentence[index][1] = "NNS";
      }
    }

    // rule 8: convert a common noun to a present participle verb (i.e., a gerund)
    function rule8(taggedSentence, index) {
      if (startsWith(taggedSentence[index][1], "NN") && endsWith(taggedSentence[index][0], "ing")) {
        taggedSentence[index][1] = "VBG";
      }
    }

    /**
     * Indicates whether or not this string starts with the specified string.
     * @param {Object} string
     */
    function startsWith($this, string) {
      if (!string) {
        return false;
      }
      return $this.indexOf(string) == 0;
    }

    /**
     * Indicates whether or not this string ends with the specified string.
     * @param {Object} string
     */
    function endsWith($this, string) {
      if (!string || string.length > $this.length) {
        return false;
      }
      return $this.indexOf(string) == $this.length - string.length;
    }

    return {
      getRule: function(index) {
        return(rules[index]);
      },
      setRule: function(index, rule) {
        rules[index] = rule;
      },
      appendRule: function(rule) {
        rules.push(rule);
      },
      setRules: function(newRules) {
        rules = newRules;
      },
      getRules: function() {
        return rules;
      }
    };

  };


  var transformationRules = BrillTransformationRules();


  function POSTagger(lexicon){
      this.lexicon = lexicon;
  }

  POSTagger.prototype.wordInLexicon = function(word){
      var ss = this.lexicon[word];
      if (ss != null)
          return true;
      // 1/22/2002 mod (from Lisp code): if not in hash, try lower case:
      if (!ss)
          ss = this.lexicon[word.toLowerCase()];
      if (ss)
          return true;
      return false;
  }

  POSTagger.prototype.tag = function(words) {
    // var taggedSentence = new Array(words.length);

    // Initialise taggedSentence with words and initial categories
    for (var i = 0, size = words.length; i < size; i++) {
      // taggedSentence[i] = new Array(2);
      words[i][0] = words[i].wrd;
      // lexicon maps a word to an array of possible categories
      var ss = this.lexicon[words[i].wrd];
      // 1/22/2002 mod (from Lisp code): if not in hash, try lower case:
      if (!ss)
        ss = this.lexicon[words[i].wrd.toLowerCase()];
      if (!ss && (words[i].wrd.length === 1))
        words[i][1] = words[i].wrd + "^";
      // We need to catch scenarios where we pass things on the prototype
      // that aren't in the lexicon: "constructor" breaks this otherwise
      if (!ss || (Object.prototype.toString.call(ss) !== '[object Array]'))
        words[i][1] = "NN";
      else
        words[i][1] = ss[0];
    }

    // Apply transformation rules
    words.forEach(function(taggedWord, index) {
      transformationRules.getRules().forEach(function(rule) {
        rule(words, index);
      });
    });
    return words;
  }

  POSTagger.prototype.prettyPrint = function(taggedWords) {
  	for (i in taggedWords) {
          print(taggedWords[i][0] + "(" + taggedWords[i][1] + ")");
      }
  }

  POSTagger.prototype.extendLexicon = function(lexicon) {
    for (var word in lexicon) {
      if (!this.lexicon.hasOwnProperty(word)) {
        this.lexicon[word] = lexicon[word];
      }
    }
  }


  function getRequest(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.addEventListener('load', (evt) => {
      callback(xhr.responseText)
    });
    xhr.open('GET', url, true);
    xhr.send();
  }

  // Tokeinze text
  function tokenize(text) {
    return text.split(SPLITS).map(w => { return w });
  };

  // Create a dictionary
  function createIndex(items) {
    var pos = {};
    var index = [];
    // var size = items.length;
    items.forEach((item, i) => {
      var e = item[1];
      if (e.length > 0) {
        if (!pos.hasOwnProperty(e)) {
          pos[e] = [];
        }
        pos[e].push([item.pos, item.pos + item.wrd.length]);
      };
      // postMessage({ type: 'prog', part: (i + 1) / size });
    });
    for (var e in pos) {
      index.push({ exp: e, cnt: pos[e].length, pos: pos[e][0][0] });
    }
    return { index: index, list: pos, type: 'data' };
  }

  var tokens = tokenize(e.data)
      tokenArray = [],
      pos = 0;
  tokens.forEach(token => {
    tokenArray.push({ wrd: token, pos: pos });
    pos += token.length;
  })

  getRequest('static/data/lexicon.en.json', data => {
    var tagger = new POSTagger(JSON.parse(data));
    var items = tokenArray.filter(e => {
      return e.wrd.trim().length > 0;
    })
    items = tagger.tag(items);
    var index = createIndex(items);
    index.codes = CODES;
    postMessage(index);
  });

}
