onmessage = function(e) {

  var [dependents, independents, maxDist] = e.data;

  function calculateRelations() {
    var start = function(info) {
      return { res: 0, sum: 0, cnt: 0, sel: info.sel }
    };
    var end = function(weight) {
      if (weight.cnt > 0) {
        weight.res = weight.sum / weight.cnt;
      }
    };
    var add = function(weight, dist) {
      weight.sum += 1 - dist / maxDist;
      weight.cnt += 1;
    };
    _calculateRelations(start, end, add);
  };

  function _calculateRelations(start, end, add) {
    var relations = dependents.map(dependent => {
      var categories = {};
      independents.forEach(independent => {
        var category = categories[independent.sel];
        if (!category) {
          categories[independent.sel] = category = start(independent);
        }
        var indeposcopy = independent.pos.map(p => { return +p }).sort((a, b) => { return a - b });
        var indepos = [];
        var depos = dependent.pos.map(p => { return +p }).sort((a, b) => { return a - b });
        depos.forEach(dep => {
          while(indeposcopy.length > 0 && indeposcopy[0] <= dep + maxDist) {
            indepos.push(indeposcopy.shift());
          }
          while(indepos.length > 0 && indepos[0] < dep - maxDist) {
            indepos.shift();
          }
          indepos.forEach(ind => {
            var dist = Math.abs(dep - ind);
            add(category, dist);
          })
        })
        end(category);
      });
      var weights = Object.values(categories);
      if (weights.length > 0) {
        var sum = weights.reduce((acc, curr) => { return acc + curr.res }, 0);
        weights.forEach(weight => { if (sum > 0) { weight.res = weight.res / sum } });
      }
      return weights;
    })

    postMessage(relations);
  }

  calculateRelations();
}
