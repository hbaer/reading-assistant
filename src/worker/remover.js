onmessage = function(e) {
  
  var list = e.data;
  var dict = {};
  var filtered = list.filter(e => {
    var s = e[0];
    var entry = dict[s];
    if (!entry) {
      dict[s] = 0;
    }
    dict[s] += 1;
    return entry == null;
  });
  postMessage(filtered);
  
}