onmessage = function(e) {
  
  const SPLITS = /([.,;:…?¿!¡"'«»‹›‘’‚“”„(){}\[\]—<>≤≥=≠+×÷∞°#&%‰@§†©®•$£€/|\\]|\-+|\*+|\s+|_+)/g;
  
  // Tokeinze text
  function tokenize(text) {
    return text.split(SPLITS).map(w => { return w });
  };

  function createDictionary(elements, n, ignore) {
    var ngrams = {}, pos = {};
    var ngram = []
    var dict = [];
    var p = 0;
    elements.forEach((e, i) => {
      if (e !== '' && !e.match(SPLITS)) {
        ngram.push([e, i, p]);
        if (ngram.length >= n) {
          var sequence = ngram.map(a => { return a[0] }).join(' ');
          var key = ignore ? sequence.toLowerCase() : sequence;
          if (!ngrams.hasOwnProperty(key)) {
            ngrams[key] = [];
            pos[key] = [];
            if (ignore) { ngrams[key].o = sequence; }
          }
          var a = ngram[0];
          ngrams[key].push(a[1]);
          pos[key].push(a[2]);
          ngram.shift();
        }
      }
      p += e.length;
    });
    for (var e in ngrams) {
      if (ngrams[e].length >= 2) {
        dict.push({ exp: ignore ? ngrams[e].o : e, cnt: ngrams[e].length, pos: pos[e][0] });
      }
    }
    return dict;
  }

  var textArray = e.data.text,
      tokenArray = [];
  textArray.forEach(text => {
    var tokens = tokenize(text);
    tokens.forEach(token => {
      tokenArray.push(token);
    })
  });

  var dict = createDictionary(tokenArray, e.data.params.n, e.data.params.ignore);
  dict.sort((a, b) => {
    return a.pos - b.pos;
  });

  postMessage(dict);

}