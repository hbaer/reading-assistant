onmessage = function(e) {

  var percentage = 0;

  var id = setInterval(() => {
    percentage += 1;
    postMessage({ cmd: 'prog', percentage: percentage });
    if (percentage >= 100) {
      postMessage({ cmd: 'end' });
      clearInterval(id);
    }
  }, 100);

}
