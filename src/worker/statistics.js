onmessage = function(e) {
  
  const WORDSPLITS = /\s+/g;
  const SENTENCESPLITS = /([.?¿!¡]|\n+|\r+)/g;
  
  // Split text into words
  function splitWords(text) {
    return text.split(WORDSPLITS);
  };

  // Split text into sentences
  function splitSentences(text) {
    return text.split(SENTENCESPLITS);
  };

  var methods = {
    countWords: function(content) {
      postMessage(splitWords(content).length);
    },
    countSentences: function(content) {
      postMessage(splitSentences(content).length);
    }
  }
  
  var content = e.data.content,
      method = e.data.method;

  methods[method](content);

}