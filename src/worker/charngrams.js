onmessage = function(e) {
  
  const BREAKS = /\s|[.,;:…?¿!¡]/;

  function createDictionary(text, n, ignore) {
    var ngrams = {}, pos = {};
    var ngram = []
    var dict = [];
    var p = 0;
    for (var c of text) {
      if (c.match(BREAKS) === null) {
        ngram.push([c, p]);
      }
      else {
        ngram = [];
      }
      if (ngram.length >= n) {
        var sequence = ngram.map(a => { return a[0] }).join('');
        var key = ignore ? sequence.toLowerCase() : sequence;
        if (!ngrams.hasOwnProperty(key)) {
          ngrams[key] = [];
          pos[key] = [];
          if (ignore) { ngrams[key].o = sequence; }
        }
        var a = ngram[0];
        ngrams[key].push(a[1]);
        pos[key].push(a[1]);
        ngram.shift();
      }
      p += 1;
    };
    for (var e in ngrams) {
      if (ngrams[e].length >= 1) {
        dict.push({ exp: ignore ? ngrams[e].o : e, cnt: ngrams[e].length, pos: pos[e][0] });
      }
    }
    dict.sort((a, b) => {
      return a.pos - b.pos;
    });
    return dict;
  }

  postMessage(createDictionary(e.data.text.join(''), e.data.params.n, e.data.params.ignore));
}