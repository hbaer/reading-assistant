onmessage = function(e) {

  const SPLITS = /([.,;:…?¿!¡"'«»‹›‘’‚“”„(){}\[\]—<>≤≥=≠+×÷∞°#&%‰@§†©®•$£€/|\\]|\-+|\*+|\s+|_+)/g;
  const SPACES = /^\s+$/;

  // Tokeinze text
  function tokenize(text) {
    return text.split(SPLITS).map(w => { return w });
  };

  // Create a dictionary
  function createDictionary(elements) {
    var words = {}, pos = {};
    var dict = [];
    var p = 0;
    elements.forEach((e, i) => {
      if (e !== '') {
        if (e.match(SPACES) === null) {
          if (!words.hasOwnProperty(e)) {
            words[e] = [];
            pos[e] = p;
          }
          words[e].push(i);
        }
        p += e.length;
      }
    });
    for (var e in words) {
      dict.push({ exp: e, cnt: words[e].length, pos: pos[e] });
    }
    return dict;
  }

  var textArray = [e.data.join('')],
      tokenArray = [];
  textArray.forEach(text => {
    var tokens = tokenize(text);
    tokens.forEach(token => {
      tokenArray.push(token);
    })
  });

  var dict = createDictionary(tokenArray);
  dict.sort((a, b) => {
    return a.pos - b.pos;
  });


  postMessage(dict);
}
