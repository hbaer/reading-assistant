onmessage = function(e) {

  var sentsep = new RegExp(/[.!?]+\s+|\n/, 'gu');

  // Create list of sentences
  function createSentenceList(text) {
    var list = [],
        match,
        p = 0;
    while ((match = sentsep.exec(text)) !== null) {
      var s = match.input.substring(p, sentsep.lastIndex).trim();
      if (s.length > 1) {
        list.push({ exp: s, cnt: 1, pos: p });
      }
      p = sentsep.lastIndex;
    }
    return list;
  }


  postMessage(createSentenceList(e.data.join('')));
}
