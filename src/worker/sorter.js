onmessage = function(e) {

  var list = e.data.list,
      key = e.data.key,
      prop = e.data.prop;

  function line0(s) {
    var pos = s.indexOf('\n');
    return pos > 0 ? s.substr(0, pos) : s;
  }

  function line1(s) {
    return s.substr(s.indexOf('\n') + 1);
  }

  function property(s, p) {
    var l = line1(s);
    if (l) {
      return JSON.parse(l)[p];
    }
  }

  var sorters = {
    '<str': function(a, b) {
      return a[0].localeCompare(b[0], 'en-US', { caseFirst: 'upper', numeric: true });
    },
    '<Str': function(a, b) {
      return a[0] < b[0] ? -1 : a[0] > b[0] ? 1 : 0;
    },
    '<num': function(a, b) {
      return +a[0] - +b[0];
    },
    '<len': function(a, b) {
      return a[0].length - b[0].length;
    },
    '>str': function(a, b) {
      return b[0].localeCompare(a[0], 'en-US', { caseFirst: 'upper', numeric: true });
    },
    '>num': function(a, b) {
      return +b[0] - +a[0];
    },
    '>len': function(a, b) {
      return line0(b[0]).length - line0(a[0]).length;
    },
    '<prp': function(a, b) {
      return property(a[0], prop) - property(b[0], prop);
    }
  };

  list.sort(sorters[key]);
  if (e.data.order) {
    list.reverse();
  }

  postMessage(list);

}
