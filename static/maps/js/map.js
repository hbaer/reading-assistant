/*
  Mapping Novels
  Institute of Cartography and Geoinformation
  H. R. Baer - hbaer@ethz.ch
*/

window.onload = function() {

  var map = L.map('map').setView([46.8, 8.2], 8);

  var osmUrl = 'https://api.mapbox.com/styles/v1/mapbox/light-v9/tiles/256/{z}/{x}/{y}@2x?access_token=pk.eyJ1IjoiaGJhZXIiLCJhIjoiY2lxNDA3N2FlMDA3Z2hybTJsM2NlMWJxbyJ9.e89LtSK6vqy_tdnmFrTIrg';
  var osmAttrib='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
  L.tileLayer(osmUrl, {
    attribution: osmAttrib
  }).addTo(map);
  
  
  // L.marker([47, 8.5]).addTo(map);
  var data = JSON.parse(document.querySelector('script#capitals').textContent);
  // console.log(data);
  L.geoJSON(data).addTo(map);
  
  // counties layer
  function popUp(feature, layer) {
    layer.bindPopup(feature.properties.NAME);
  }
    
    
    
    
    
}
