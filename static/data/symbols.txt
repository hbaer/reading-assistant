name	color	attr
Protagonist	#25c700	
Minor character	#cbe9a5	
Antagonist	#c553f3	
Friend	#ff722f	
Enemy	#792484	
Man	#e0292c	
Woman	#106cd4	
Child	#d3df08	
Adult	#737373	
Direct speech	#fefab9	
Animal	#8d470d	
Spring	#a3e92c	
Summer	#f0f11b	
Autumn	#ff7e00	
Winter	#78ecf1	
Time	#99d4ff	
Morning	#5ff012	
Noon	#e93a14	
Afternoon	#935100	
Evening	#942092	
Night	#011892	
Midnight	#001d57	
Dawn	#42839f	
Country	#659c34	
City	#e22400	
Village	#e58f00	
Highway	#ffcf00	
Road	#ba5329	
Street	#aaaaaa	
Place	#d29d00	
Railway	#e22400	
Nature	#a9cf21	
Mountain	#b78d48	
Hill	#57bc0f	
Pass	#375719	
Island	#52e21d	
Ocean	#005392	
Lake	#78d5f0	
River	#046cce	
Love	#e22400	
Romance	#f0c9fe	
Sadness	#95119d	
Seriousness	#003c7f	
Envy	#b9f3fe	
Pleasure	#ff6b00	
Deceit	#b7004d	
Greed	#007800	
Jealousy	#fff000	
Joy	#fefab9	
Grief	#222222	
Red	#ED0A3F	
Maroon	#C32148	
Scarlet	#FD0E35	
Vermilion	#CC474B	
Orange-red	#FF5349	
Venetian red	#CC553D	
Red-orange	#FF681F	
Orange	#FF8833	
Yellow-orange	#FFAE42	
Orange-yellow	#F8D568	
Yellow	#FBE870	
Green-yellow	#F1E788	
Olive green	#B5B35C	
Yellow-green	#C5E17A	
Green	#3AA655	
Teal blue	#008080	
Light blue	#8FD8D8	
Aquamarine	#95E0E8	
Turquoise blue	#6CDAE7	
Sky blue	#76D7EA	
Blue-green	#0095B7	
Green-blue	#2887C8	
Navy blue	#0066CC	
Denim	#1560BD	
Cadet blue	#A9B2C3	
Indigo	#4F69C6	
Cobalt blue	#8C90C8	
Celestial blue	#7070CC	
Violet-blue	#766EC8	
Blue-violet	#6456B7	
Ultramarine blue	#3F26BF	
Royal purple	#6B3FA0	
Fuchsia	#C154C1	
Violet	#732E6C	
Red-violet	#BB3385	
Magenta	#F653A6	
Violet-red	#F7468A	
Carmine	#E62E6B	
Blush	#DB5079	
Sienna	#E97451	
Brown	#AF593E	
Sepia	#9E5B40	
Tan	#D99A6C	
Umber	#805533	
Gold	#E6BE8A	
Silver	#C9C0BB	
Copper	#DA8A67	
Black	#000000	
Charcoal gray	#736A62	
Gray	#8B8680	
Blue-gray	#C8C8CD	