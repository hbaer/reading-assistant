# Reading Assistant

#### 2018-5-25
Word plot and text section sorting

* By text order

#### 2018-5-22
Text sections

* ~~Show labels (possibly top right)~~
* ~~Change size of row~~
* ~~Allow to interactively manipulate the rows' positions and heights~~ (realized by attributes)

Panel "Manage Spatial Data"

* ~~Show unused features~~
* Keep title in place (possible with display: table?)
* Selected check boxes are not sorted

#### 2017-9-24
HTTP request for HTML document cannot get images with relative path names.

#### 2017-8-15

* Handle error: "QuotaExceededError (DOM Exception 22): The quota has been exceeded."
* "Close without saving" appears even after saving
* Highlighter's dataset value can be wrong (after import?)
* Accessor can be undefined (textdata.js 212)

#### 2017-8-13

* SVG in iOS Safari not scrolling (WordPlot and TextSections)

#### 2017-8-8
Changes to selections

* <del>Removing text selection should only remove previously created span elements</del>
* Changing the color of a label entry should also change the color of the text highlighter
* <del>Performing multiple selection changes to the current color</del>

#### 2017-8-7
Index entry selections

* Shift click to add selections
* Select all entries as menu item
* Remove selected entries

#### 2017-7-17
Apply highlighters does not apply
Word plot: sort by number of occurrences OR by given order

#### 2017-7-7
Word selections

* Full word
* Start as word
* End as word

Update color when pressing "selection"?

#### 2017-7-4
Finally, selecting text by sequences is working.

* Overlapping selections not (yet) solved.
* Changes in the highlighters are not reflected in the selection tool
* Colors are only visible after the highlighter panel was displayed

#### 2017-7-3
selectWordsInText2 not working properly.
Styles for highlighters only created when highlighters dialog opened.

#### 2017-7-1
Index: How to select text?

* Nothing
* Sequences
* Words
* Regexp

#### 2017-6-19
Plot filename undefined (Firefox)
Missing font (Avenir) on Windows 10

#### 2017-6-18
Cannot move plain text to new window.
